import React, { Component } from 'react';
import './App.css';
import { Header, Notes, EditNote, Search } from './components';
import axios from 'axios';

//on notes edit={this.edit}
class App extends Component {
     
    api = 'http://localhost:3006/notes';

    state = {
        filtered: [],
        notes: [],
        showEdit: false,
        editId: '',
        search: ''
    }

    componentDidMount() {
        axios.get(this.api, { crossdomain: true },)
        .then(res => this.setState({ notes: res.data, filtered: res.data }))
    }

    updateNote = (body, id) => {
       const url = `${this.api}/${id}`;
        axios.patch(url, { body })
        .then(res => {
            console.log(this.state.notes.length);
            const ind = this.state.notes.findIndex(n => n.id === res.data.id);
            const notes = this.state.notes;
            notes[ind] = res.data;
            this.setState({ notes: notes, editId: '', showEdit: false });
            console.log(this.state.notes.length);
        })
    }

    cancelUpdate = () => {
        this.setState({ editId: '', showEdit: false })
    }

    editNote = (id) => {
        this.setState({ showEdit: true, editId: id });
    } 

    searchNotes = (search) => {
        search = search.toLowerCase();
        this.setState({ search })
        if (this.state.search !== search) {
            const newFiltered = this.state.notes.filter(n => n.author.toLowerCase().indexOf(search) > -1);
            // temp1.filter(n => n.author.indexOf('h') > -1)
            this.setState({ search, filtered: newFiltered });
        }
    }

    render() {
        return (
            <div className="container" style={{maxHeight: '100vh'}}>
                <div style={{height: '30vh'}}>
                    <Header />
                    <Search searchNotes={this.searchNotes} search={this.state.search}></Search>
                </div>
                <div style={{height: '70vh', overflowY: 'scroll'}}>
                    {this.state.showEdit && (
                        <EditNote cancelUpdate={this.cancelUpdate} updateNote={this.updateNote} id={this.state.editId}/>
                    )}
                    <Notes notes={this.state.filtered} editNote={this.editNote}/>
                </div>
            </div>
        )
    }
}

export default App;