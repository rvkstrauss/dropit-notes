import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './Note.css';

export class Note extends Component {

  render() {
    const { id, author, description} = this.props.note;
    return (
      <div key={id} className="note" >
        <div className="note-details">
            <div>
                <h3>Author name:</h3>
                <h3>Description:</h3>
            </div>
            <div>
                <h3 className="note-description">{author}</h3>
                <div className="note-description text">{description}</div>
            </div>
        </div>
        <div className="btn-section">
            <button className="btn" onClick={this.props.editNote.bind(this, id)}>
                <span className="edit-btn"><FontAwesomeIcon icon={faPen}/></span>
            </button>
        </div>
      </div>
    )
  }
}

// PropTypes
Note.propTypes = {
  note: PropTypes.object.isRequired,
  editNote: PropTypes.func.isRequired,
}

export default Note