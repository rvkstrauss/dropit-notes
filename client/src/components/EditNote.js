import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import './EditNote.css';

export class EditNote extends Component {
  
  _isMounted = false;
  api = 'http://localhost:3006/notes'
  
  state = {
    note: {},
    body: ''
  }

  componentDidMount() {
    this._isMounted = true;
    this.setNote();
  }

  setNote() {
    const url = `${this.api}/${this.props.id}`;
    axios.get(url, { crossdomain: true })
    .then(res => {
        if(this._isMounted){
            this.setState({ note: {...res.data}, body: res.data.body })}
        }
    );
  }

  componentDidUpdate(prevProps) {
    if (prevProps.id !== this.props.id) {
      this.setNote();
    }
  }

  parseDateString(d) {
      if (!d)
         d = new Date(this.state.note.updatedAt);
      else {
          d = new Date(d);
      }
      let vals = [
        d.getDate().toString().padStart(2, '0'), 
        d.getMonth().toString().padStart(2, '0'), 
        d.getFullYear()
      ]
      return vals.join('/');
      // const day = d.getDay().toString().padStart(1, '0');
      // console.log(day);
      // return `${d.getDate()}/${d.getMonth()}/${d.getFullYear()}`
  }

  onChange = (e) => this.setState({ [e.target.name]: e.target.value });

  render() {
    const {author, body, description, updatedAt} = this.state.note;
    return (         
      <div className="overlay">
            <div className="edit-modal">
            <div style={{width: '100%', textAlign: 'right', height: '21px'}}>
                <h4 style={{fontWeight: 100}}>Updated at: {this.parseDateString(updatedAt)}</h4>
            </div>
            <div className="modal-content">
                <div style={{width: '29%'}}>
                    <h3>Author name: </h3>
                    <h3>Description: </h3>
                    <h3 style={{width: '80%', marginTop: '40px'}}>Body: </h3>
                </div> 
                <div style={{width: '80%', marginLeft: '5%'}}>
                    <h3 style={{fontWeight: 100}}>{author}</h3> 
                    <p style={{fontSize: '13px'}}>{description}</p> 
                    <textarea 
                        style={{marginTop: '48px'}}
                        type="text" 
                        name="body" 
                        className="edit-body"
                        value={this.state.body}
                        onChange={this.onChange}
                    />
                </div>
            </div>
            <div className="btns">
                <button className="btn cancel" onClick={this.props.cancelUpdate.bind(this)}>Cancel</button>
                <button className="btn save" onClick={this.props.updateNote.bind(this, this.state.body, this.props.id)}>Save</button>
            </div>
        </div>
    </div>
    )
  }
}

// PropTypes
EditNote.propTypes = {
    id: PropTypes.string.isRequired,
    cancelUpdate: PropTypes.func.isRequired,
    updateNote: PropTypes.func.isRequired
}

export default EditNote