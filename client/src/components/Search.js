import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

export class Search extends Component {

    state = {
        search: ''
    }

  componentDidMount () {
      this.setState({search: this.props.search})
  }

  onChange = (e) => {
      e.preventDefault();
      this.setState({ [e.target.name]: e.target.value });
      this.props.searchNotes(e.target.value);
  }

  render() {
    return (
        <div style={{ marginLeft: '20%', position: 'fixed', top: '16%', left: '0%' }}>
            <input 
                style={searchStyle} 
                placeholder={'Search...'}
                type="text" 
                name="search" 
                value={this.state.search}
                onChange={this.onChange}
            />
            <FontAwesomeIcon style={searchIcon} icon={faSearch}></FontAwesomeIcon>
        </div>
    )
  }
}

Search.propTypes = {
    search: PropTypes.string,
    searchNotes: PropTypes.func.isRequired
}

const searchStyle = {
    background: '#C0C0C0',
    padding: '9px 0px 9px 28px',
    fontSize: '1rem',
    fontWeight: 400,
    color: 'white',
    borderRadius: '15px',
    border: 'none',
    margin: '30px'
}

const searchIcon = {
    float: 'right',
    fontSize: '15px',
    marginRight: '6px',
    marginTop: '-20px',
    position: 'relative',
    zIndex: 2,
    color: 'white',
    right: '79%',
    top: '62px',
}
export default Search;
