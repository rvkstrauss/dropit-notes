import React from 'react';
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Header() {
  return (
    <header style={headerStyle}>
        <FontAwesomeIcon icon={faUser}/><span style={nameStyle}>Rivka Strauss</span>
    </header>
  )
}

const headerStyle = {
    background: '#EBEBEB',
    flex: '1 1 auto',
    padding: '40px',
    fontSize: '1.8rem',
    color: '#585858',
    fontWeight: 600
}

const nameStyle = {
    marginLeft: '10px'
}

// const headerStyle = {
//   background: '#333',
//   color: '#fff',
//   textAlign: 'center',
//   padding: '10px'
// }

// const linkStyle = {
//   color: '#fff',
//   textDecoration: 'none'
// }

export default Header;
