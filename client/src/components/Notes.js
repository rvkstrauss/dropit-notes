
import React, { Component } from 'react';
import Note from './Note';
import PropTypes from 'prop-types';

class Notes extends Component {
  render() {
      return this.props.notes.map((note) => (
        <div style={notesContainer}>
            <Note key={note.id} note={note} editNote={this.props.editNote} />
        </div>
    ));
  }
}

// PropTypes
Notes.propTypes = {
  notes: PropTypes.array.isRequired,
  editNote: PropTypes.func.isRequired,
}

const notesContainer = {
    display: 'flex',
    flexFlow: 'column no-wrap',
    justifyContent: 'center',
}

export default Notes;
